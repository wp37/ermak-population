<?php
	class Ermak_Population
	{
		public $options;
		function __construct()
		{
			//return;
			add_action( 'admin_menu',								array($this, 'admin_page_handler'), 9);
			add_action( 'admin_enqueue_scripts', 					array($this, 'admin_scripts') );	
			add_action( 'wp_enqueue_scripts', 						array($this, 'front_script_add') );
			add_action( 'smc_add_object_type',						array(__CLASS__, 'smc_add_object_type') , 15);
		}
		function admin_page_handler()
		{
			add_menu_page( 
						__('Ermak. Population', "ermak_pop"), // title
						__('Ermak. Population', "ermak_pop"), // name in menu
						'manage_options', // capabilities
						'Ermak_population_page', // slug
						array($this, 'ermak_population_setting_pages'), // options function name
						ERMAK_POPULATION_URLPATH .'img/ermak.png', // icon url  
						'20.401'
						);
		}
		function ermak_population_setting_pages()
		{
			$html		= "<h1>".__("Ermak Population. Settings","ermak_pop")."</h1>";
			echo $html;
		}
		
		function admin_scripts()
		{					
			wp_register_script('ermak_pop_admin', plugins_url( '../js/ermak_pop_admin.js', __FILE__ ), array());
			wp_enqueue_script('ermak_pop_admin');
			
			wp_register_style( 'ermak_pop_admin-style', plugins_url( '../css/ermak_pop_admin.css', __FILE__ ), array());
			wp_enqueue_style ( 'ermak_pop_admin-style' );
		}
		function front_script_add()
		{
			//js
			wp_register_script('ermak_pop_front', plugins_url( '../js/ermak_pop_front.js', __FILE__ ), array());
			wp_enqueue_script('ermak_pop_front');		
			
			wp_register_style( 'ermak_pop_front-style', plugins_url( '../css/ermak_pop_front.css', __FILE__ ), array());
			wp_enqueue_style ( 'ermak_pop_front-style' );
				
		}
		static function smc_add_object_type($array)
		{
			$epg								= array();
			$epg['t']							= array('type' => 'post');
			$epg['consume_scheme_id']			= array('type' => 'id', 'object' => SMCO_CONSUME_SCHEME_TYPE);
			$epg['picto_id']					= array('type' => 'media', 	'download'=>true);
			$array[ERMAK_POPULATION_GROUP_TYPE]	= $epg;
			
			return $array;
		}
	}
?>