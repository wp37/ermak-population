<?php
	class Ermak_Population_Group extends SMC_Post
	{
		static function init()
		{
			add_action( 'init', 											array(__CLASS__, 'add_ermak_population_group'), 16);			
			add_action( 'save_post_'.ERMAK_POPULATION_GROUP_TYPE,			array(__CLASS__, 'true_save_box_data_ermak_population_group'));
			add_action( 'admin_menu',										array(__CLASS__, 'my_extra_fields_ermak_population_group'));
			add_filter( 'manage_edit-'.ERMAK_POPULATION_GROUP_TYPE.'_columns',			array(__CLASS__, 'add_views_column_ermak_population_group'), 4);
			add_filter( 'manage_edit-'.ERMAK_POPULATION_GROUP_TYPE.'_sortable_columns',	array(__CLASS__, 'add_views_sortable_column_ermak_population_group'));
			add_filter( 'manage_'.ERMAK_POPULATION_GROUP_TYPE.'_posts_custom_column',	array(__CLASS__, 'fill_views_column_ermak_population_group'), 5, 2); 
			
			add_filter('ermak_population_hint',								array(__CLASS__, 'ermak_population_hint'), 10, 4);
			
			add_action( SMC_LOCATION_NAME.'_edit_form_fields', 				array(__CLASS__, 'edit_location_type_to_location'), 9, 2 );
			add_filter("smc_location_tab_over_params",						array(__CLASS__, 'smc_location_tab_over_params'), 90, 3);
			add_filter( "open_".SMC_LOCATION_NAME."_content_widget",		array(__CLASS__, 'open_location_content_widget'), 12, 2);
			add_action( 'edit_'.SMC_LOCATION_NAME, 							array(__CLASS__, 'save_taxonomy_custom_meta'), 11);  
			add_action( 'create_'.SMC_LOCATION_NAME, 						array(__CLASS__, 'save_taxonomy_custom_meta'), 11);
			
			add_action("smco_control_consume", 								array(__CLASS__, "smco_control_consume"), 10, 3);
			add_filter("scmo_get_consume_id",								array(__CLASS__, "scmo_get_consume_id"), 10, 3);
			add_filter("smco_consume_batch_filter",							array(__CLASS__, "smco_consume_batch_filter"), 10, 5);
			add_filter("smp_calc_location_needs",							array(__CLASS__, "smp_calc_location_needs"), 2, 2);
			
			add_action( 'smc_add_option',									array(__CLASS__, 'smc_add_option') , 13);
			add_action( 'smc_add_object_type',								array(__CLASS__, 'smc_add_object_type') , 13);
			
			add_filter("location_display_tbl1", 							array(__CLASS__, "location_display_tbl1"), 19, 4);
		}
		static function add_ermak_population_group()
		{
			$labels = array(
					'name' => __('Population Group', "ermak_pop"),
					'singular_name' => __("Population Group", "ermak_pop"), // ����� ������ ��������->�������
					'add_new' => __("add Population Group", "ermak_pop"),
					'add_new_item' => __("add new Population Group", "ermak_pop"), // ��������� ���� <title>
					'edit_item' => __("edit Population Group", "ermak_pop"),
					'new_item' => __("add Population Group", "ermak_pop"),
					'all_items' => __("all Population Groups", "ermak_pop"),
					'view_item' => __("view Population Group", "ermak_pop"),
					'search_items' => __("search Population Group", "ermak_pop"),
					'not_found' =>  __("Population Group not found", "ermak_pop"),
					'not_found_in_trash' => __("no found Population Group in trash", "ermak_pop"),
					'menu_name' => __("Population Group	", "ermak_pop") // ������ � ���� � �������
				);
				$args = array(
					 'label'			=> ERMAK_POPULATION_GROUP_TYPE
					,'labels' 			=> $labels
					,'description'		=> __("", "ermak_pop")
					,'public' 			=> true
					,'show_ui' 			=> true // ���������� ��������� � �������
					,'has_archive' 		=> true 
					,'exclude_from_search' => true
					,'menu_position' 	=> 17 // ������� � ����
					,'show_in_menu' 	=> "Ermak_population_page"
					,'supports' 		=> array(  'title', 'thumbnail')
					//,'capabilities' 	=> 'manage_options'
					,'capability_type' 	=> 'post'
				);
				register_post_type(ERMAK_POPULATION_GROUP_TYPE, $args);
		}
		
		static function get_type()
		{
			return ERMAK_POPULATION_GROUP_TYPE;
		}
		function get_class_name()
		{
			return "Ermak_Population_Group";
		}
		static function is_consume()
		{
			return is_plugin_active('Ermak_consume/Ermak-consume.php');
		}
		static function my_extra_fields_ermak_population_group() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(__CLASS__, 'extra_fields_box_func_Population_Group'), ERMAK_POPULATION_GROUP_TYPE, 'normal', 'high'  );
		}
		static function extra_fields_box_func_Population_Group( $post )
		{
			global $Soling_Metagame_Constructor;
			$gunner		= self::get_instance($post->ID);
			$picto		= $gunner->get_picto_id();
			?>
			<script>
				jQuery(document).ready(function( $ )
				{	
					$("#delete_picto_id").click(function(e)
					{
						$("#pictogramm").empty().append("<img style='opacity:0.5;' src='<?php echo ERMAK_POPULATION_URLPATH; ?>img/population_group.png'/>");
						$("#picto_media_id").val("");
					});
					jQuery( "#my_image_upload" ).click(function() 
					{
						//alert( $("#post_id").val() );
						//send(['upload_media']);
						on_insert_media = function(json)
						{
							//alert(json.url);
							$("#picto_media_id").val(json.id);
							$("#pictogramm").empty().append("<img src='"+json.url+"'>");
						}
						open_media_uploader_image();						
					});
				});
			</script>
			<div class='smc-cont'>							
				<div class="smp_batch_extra_field_column">
					<div class="h">
						<board_title><?php echo __("Consume scheme", "ermak_pop") ;?></board_title><br>
						<div class="styled-select state rounded">
						<?php 
						if(!self::is_consume())
						{
							echo __("Install Ermak Consume plugin to be able to manage consumption.", "ermak_pop");
						}
						else
						{
							echo SMCO_Assistant::wp_drp_all_consumer_schemes( array('selected' => $gunner->get_consume_scheme_id(), 'type' => 'user',  'name' => "consume_scheme_id", 'id' => "consume")); 
						}
						?>	
						</div>						
					</div>			
					<div class='h'>
						<board_title><?php echo __("Pictogramm", "ermak_pop") ;?></board_title><br>
						<div id="my_image_upload" class='button' style='padding:10px; height:120px; float:left; margin-right:3px;'>
							<div id='pictogramm' style='position:relative; display:inline-block; height:100px; overflow:hidden;'>
							<?php echo $gunner->get_pictogramm(); ?>
							</div>
						</div>
						<div style="height:120px;">
							<div class="button" id='delete_picto_id'><?php _e("Clear");?></div>
							<p class='description'><?php _e("Height - 100 pixel", "ermak_pop"); ?></P>
						</div>
						<input name='picto_media_id' id="picto_media_id" type="hidden" value='<?php echo $gunner->get_picto_id(); ?>'/>
					</div>
				</div>
			</div>
			<?php
			wp_nonce_field( basename( __FILE__ ), 'goods_ca_type_metabox_nonce' );
		}
		
		static function true_save_box_data_ermak_population_group ( $post_id) 
		{					
			//self::insert_gunner($post_id, get_post($post_id), false);
			global $table_prefix, $SolingMetagameProduction;
			// ���������, ������ �� ������ �� �������� � ����������
			if ( !isset( $_POST['goods_ca_type_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['goods_ca_type_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// ���������, �������� �� ������ ���������������
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// ���������, ����� ������������, ����� �� �� ������������� ������
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;				 	
			if(static::is_consume())
				update_post_meta($post_id, 'consume_scheme_id', $_POST['consume_scheme_id']);			
				update_post_meta($post_id, 'picto_id', 			$_POST['picto_media_id']);			
			return $post_id;
		}
		
		static function add_views_column_ermak_population_group( $columns )
		{
			//$columns;
			$posts_columns = array(
				  "cb" 					=> " ",
				  "IDs"					=> "ID",
				  "title"				=> __("Title"),
				  "pictogramm"			=> __("Image")
			   );
			if(static::is_consume())
				$posts_columns[ "consume_scheme_id" ]		= __("Consume", "ermak_pop");
			return $posts_columns;			
		}
		// ��������� ����������� ����������� �������
		static function add_views_sortable_column_ermak_population_group($sortable_columns){
			$sortable_columns['IDs'] 					= 'ids';
			$sortable_columns['pictogramm'] 			= 'pictogramm';
			if(static::is_consume())
				$sortable_columns['consume_scheme_id'] 	= 'consume_scheme_id';
			$sortable_columns['title'] 					= 'title';
			return $sortable_columns;
		}	
		// ��������� ������� �������	
		static function fill_views_column_ermak_population_group($column_name, $post_id)
		{
			$post			= self::get_instance($post_id);
			switch( $column_name) 
			{				
				case "IDs":	
					echo $post_id;
					break;
				case "pictogramm":	
					echo $post->get_pictogramm();
					break;
				case "consume_scheme_id":
					$t		= $post->get_consume_scheme();
					echo $t;
					break; 		
			}		
		}
		
		static function save_taxonomy_custom_meta($location_id)
		{
			global $wpdb;
			if($_POST)
			{
				$arr	= array();
				for($i=0; $i<$_POST['col_pop']; $i++)
				{
					$type		= $_POST['pop_type_'.$i];
					$count		= $_POST['pop_num_'.$i];
					if($type==-1 || $count == 0) continue;
					$arr[]		= array('type'=>$type, 'count'=>$count);
				}			
				//remove
				$rem_query		= "DELETE FROM " . $wpdb->prefix . "ermak_population WHERE location_id=$location_id";
				$wpdb->query( $rem_query );
				
				//insert
				$add_query						= "REPLACE INTO " . $wpdb->prefix . "ermak_population (location_id,type,count) VALUES ";
				$adds							= array();
				foreach($arr as $u)
				{
					$adds[]						= '('.$location_id.','.$u['type'].','.$u['count'].')';
				}
				$add_query						.= implode($adds, ",");
				$wpdb->query( $add_query );
			}
		}
		static function get_population($location_id)
		{
			global $wpdb;
			return $wpdb->get_results("SELECT * FROM ".$wpdb->prefix . "ermak_population WHERE location_id=$location_id ORDER BY ID", OBJECT );
		}
		static function get_loc_population_structure($loc_meta)
		{
			$loc_meta['population_structure']	= stripslashes ($loc_meta['population_structure']);
			$population_groups	= json_decode($loc_meta['population_structure']);
			if(!is_array($population_groups))	$population_groups = array();
			return $population_groups;
		}
		
		static function edit_location_type_to_location($term, $tax_name)
		{
			$t_id = $term->term_id;	 
			$term_meta = SMC_Location::get_term_meta( $t_id );
			//$population_groups	= self::get_loc_population_structure($term_meta);
			$population_groups		= self::get_population($t_id);
			$population_frontal_consume	=	(int)$term_meta['population_frontal_consume'];
			$i=0;
			if(count($population_groups))
				foreach($population_groups as $group)
				{
					$html	.= "
					<div id='popgroup_$i' style='margin-top:2px;'>".
						self::wp_dropdown( array( 'name'=>"pop_type_$i", 'class'=>'', 'style'=>'float:left; margin-right:13px; width:200px;', 'selected'=>$group->type) ). 
						"<input type='number' value='".$group->count."' name='pop_num_$i' class='pop_num' min='0' style='width:80px; margin:0 2px;' />
						<span class='button remove_popgroup'>-</span>
					</div>";
					$i++;
				}
			echo apply_filters("ermak_population_edit_location_block", 
			"<tr class='form-field'>
				<th scope='row' valign='top'>
					<label for='term_meta[population]'>". __('Structure of Population', "ermak_pop") . "</label></th>
				</th>
				<td>
					<div>
						<table>
							<tr>
								<td>
								<div id='pop_cont' class='h7'>
									<input name='col_pop' value='$i' type='hidden'/>
									$html
									<div class='lp-hide' id='popgroup_' style='margin-top:2px;'>".
										self::wp_dropdown( array( 'name'=>'pop_type', 'class'=>'chosen-select11', 'style'=>'float:left; margin-right:13px; width:200px;' ) ).
										"<input type='number' value='0' name='pop_num' min='0' style='width:80px; margin:0 2px;' class='pop_num'/>
										<span class='button remove_popgroup'>-</span>
									</div>
									<div class='button' id='add_popgroup' style='margin-top:10px;'>+</div>
								</div>
								<div class='h7'>
									<input type='radio' name='term_meta[population_frontal_consume]' ".checked(0, $population_frontal_consume, 0)." id='population_frontal_consume1' class='css-checkbox' value='0'/>
									<label for='population_frontal_consume1' class='css-label'>".__("We buy everything that belongs to our Location.", "ermak_pop")."</label><BR>
									<input type='radio' name='term_meta[population_frontal_consume]' ".checked(1, $population_frontal_consume, 0)." id='population_frontal_consume2' class='css-checkbox' value='1'/>
									<label for='population_frontal_consume2' class='css-label'>".__("We buy everything that is sold in our Location.", "ermak_pop")."</label>
								</div>
								</td>
							</tr>
						</table>
					</div>	
					<!--input type='hidden' id='population_structure' name='term_meta[population_structure]' value='".$term_meta['population_structure']."'/-->".
					//Assistants::echo_me( json_decode( stripslashes ($term_meta['population_structure']) ), true ) .
				"</td>
			</tr>
			<script>
				jQuery(document).ready(function( $ )
				{
					var n		= parseInt($('[name=col_pop]').val());
					$('#add_popgroup').click(function(e)
					{
						$('#popgroup_').
							clone().
								appendTo($('#pop_cont')).
									fadeIn('slow').
										attr('id', 'popgroup_'+n);
						$('#popgroup_'+n).children('[name=pop_type]').attr('name', 'pop_type_'+n);
						$('#popgroup_'+n).children('[name=pop_num]' ).attr('name', 'pop_num_'+n);
						$('#pop_cont').append($(this));
						$('[name=col_pop]').val(n + 1);
						n = parseInt($('[name=col_pop]').val());
						
					});
					$('.remove_popgroup').live({'click':function(e)
					{
						$('[name=col_pop]').val(n - 1);
						n = parseInt($('[name=col_pop]').val());
						$(this).parent().detach();
						
					}});
					$('.pop_num').live({'click':function(e)
					{
						var arr		= new Array();
						for(var i=0; i<n; i++)
						{
							var id 		= $('[name=pop_type_'+ i + ']').val();
							var num		= $('[name=pop_num_' + i + ']').val();
							if(id ==-1 || num==0) continue;							
							var obj		= new Object();
							obj.type	= id;
							obj.count	= num;
							arr[i]		= obj;
						}
						$('#population_structure').val(JSON.stringify(arr));
					}});
				});
			</script>
			", $term, $term_meta);
		}
		static function get_location_population_form($location_id)
		{
			$pfc		 		= self::get_population_frontal_consume($location_id);
			$con				= !$pfc ? __("We buy everything that belongs to our Location.", "ermak_pop") : __("We buy everything that is sold in our Location.", "ermak_pop");
			//$population			= $meta['population'];
			//$population_structure	= self::get_loc_population_structure($meta);
			$population_structure	= self::get_population($location_id);
			//return Assistants::echo_me($population_structure);
			if(count($population_structure)==0)	return "";
			foreach($population_structure as $pop)
			{
				$pops			= self::get_instance( $pop->type );
				$html			.= $pops->get_group_pictogramm($pop->count, $location_id);
			}
			return "
			<div style='font-size:11px; color:#AAA; font-family:Open Sans, Arial; display:inline-block; position:relative; background:#444; width:90%'>".
				"<div style='text-transform: uppercase; padding:3px 10px; background-color:#111;'>".
					__("Population", "ermak_pop") . ":</div> 
				<div style='display: inline-block; clear: right; position: relative; padding:0px 6px;'>
					$html
				</div>
				<div style='padding:0px 6px;'>
					$con
				</div>
			</div>";
		}
		
		static function smc_location_tab_over_params($text, $location_id, $loc_option)
		{
			return self::get_location_population_form($location_id). $text;
		}
		static function open_location_content_widget($text, $params)
		{
			$location_id		= $params[1];
			//$loc_meta			= SMC_Location::get_term_meta($location_id);
			//$population_structure	= self::get_loc_population_structure($loc_meta);
			$population_structure	= self::get_population($location_id);
			if(count($population_structure)==0)	return $text;
			$html				= "<div class='klapan3_subtitle lp_clapan_raze'>".					
					"<div style='position:relative; display:block;'>".
						__("Population", "ermak_pop") . 
					'</div>
				<div class="lp_clapan_raze1"></div>
				</div>
				<div class="smp_bevel_form">'.
					self::get_location_population_form($location_id).
				"</div>";
			$arr				= array("title" => "<img src=".ERMAK_POPULATION_URLPATH."img/population_picto.png>", 	"slide" => $html, 'hint' => __("Population", "ermak_pop"));
			//return $text;
			$args1 				= array_slice  ($text, 1);
			$args  = array_merge(
									array(
											$text[0],
											$arr
										 ), 
									$args1
								);
			return $args;
		}
		
		
		function get_consume_scheme_id()
		{
			return $this->get_meta("consume_scheme_id");
		}
		function get_consume_scheme($count=1, $currency_type_id=-1)
		{
			global	$Ermak_Consume;
			return $Ermak_Consume->admin_consume_form( $this->get_consume_scheme_id(), true, $count, false, $currency_type_id );
		}
		function get_picto_id()
		{
			return $this->get_meta("picto_id");
		}
		function get_pictogramm($size=100)
		{
			$picto_id	= $this->get_picto_id();
				
			if($picto_id)
			{
				return "<img style='height:".$size."px; width:auto;' src='".wp_get_attachment_image_url($picto_id, array($size, $size))."'/>";
			}
			else
			{
				return "<img style='opacity:0.5; height:".$size."px; width:auto;' src='".ERMAK_POPULATION_URLPATH."img/population_group.png'/>";
			}
		}
		function get_group_pictogramm( $count, $location_id, $size=50 )
		{
			return "
			<div class='div_hinter' data-hint='pop_group_".$this->id."' style='float:left; margin:2px;'>
				<div style='position: relative;'>".
					$this->get_pictogramm($size).
					"<div class='pop_group_count'>$count</div>
				</div>
			</div>".$this->get_hint(array("count"=>$count, "location_id"=>$location_id));
		}
		static function ermak_population_hint($text, $id, $count, $location_id)
		{
			$pg			= self::get_instance($id);
			$meta		= SMC_Location::get_term_meta($location_id);
			$dct_id		= $meta['currency_type'];
			return $text . $pg->get_consume_scheme( $count, $dct_id );
		}
		
		static function get_location_population_table($location_id)
		{
			$pfc		 		= self::get_population_frontal_consume($location_id);
			$con				= !$pfc ? __("We buy everything that belongs to our Location.", "ermak_pop") : __("We buy everything that is sold in our Location.", "ermak_pop");
			//$population			= $meta['population'];
			//$population_structure	= self::get_loc_population_structure($meta);
			$population_structure	= self::get_population($location_id);
			//return Assistants::echo_me($population_structure);
			if(count($population_structure)==0)	return "";
			foreach($population_structure as $pop)
			{
				$pops			= self::get_instance( $pop->type );
				$html			.= $pops->get_hint(array("count"=>$pop->count, "location_id"=>$location_id), false);
				//$html			.= $pops->get_group_pictogramm($pop->count, $location_id, 70);
			}
			return "
			<div style='font-size:11px; color:#EEE; font-family:Open Sans, Arial; display:inline-block; position:relative; background:#444; width:100%'>".
				"<div style='text-transform: uppercase; padding:3px 10px; background-color:#111; font-size:20px;'>".
					__("Population", "ermak_pop") . ":</div> 
				<div style='display: inline-block; clear: right; position: relative; padding:0px 6px;'>
					$html
				</div>
				<div style='padding:0px 6px; font-size:12px;'>
					$con
				</div>
			</div>";
		}
		function get_hint($params=-1, $hidden=true)
		{
			if(!is_array($params))
				$params	= array("count"=>1);
			$count		= $params['count'];
			$location_id= $params['location_id'];
			$hidden_class = $hidden ? "lp-hide" : "";
			$html		= "
			<div class='pop_hint_table $hidden_class' id='pop_group_".$this->id."' style='width:390px;'>
				<table>
					<tr>
						<td rowspan='3' style='vertical-align:top;'>
							<div style='position:relative;display:inline-block;'>".
								$this->get_pictogramm(100).
						"		<div style='position:absolute; bottom:4px; left:0; color:#FFF; background:red; padding:2px 7px;'>$count</div>
							</div>
						</td>
						<td style='width:290px;'>
							<board_title>".$this->get("post_title")."</board_title>
						</td>
					</tr>
					<tr>
						<td style='font-size:13px;color:#333;'>".
							 sprintf(__("%s men", 'ermak_pop'), $count).
						"</td>
					</tr>
					<tr>
						<td>".
							apply_filters("ermak_population_hint", " ", $this->id, $count, $location_id).
						"</td>
					</tr>
				</table>
			</div>";
			return $html;
		}
		
		static function get_population_frontal_consume($location_id)
		{
			$meta				= SMC_Location::get_term_meta($location_id);
			return $meta['population_frontal_consume'];
		}	
		static function smco_control_consume( $id, $consumer_type, $current_circle_id )
		{
			global $Ermak_Consume, $RPT;
			switch($consumer_type)
			{
				case SMC_LOCATION_NAME:
					$meta								= SMC_Location::get_term_meta($id);
					$pfc		 						= $meta['population_frontal_consume'];
					$population_structure				= self::get_population($id);
					if(count($population_structure)==0)	return array();
					foreach($population_structure as $pop)
					{
						$pops							= self::get_instance( $pop->type );
						$RPT							.=  "
						<table>
							<tr>
								<td style='width:120px;'>
									<span style='display:inline-block; position:relative;'>".$pops->get_group_pictogramm($pop->count, $id, 100)."</span>
								</td>
								<td>".
									__( "Population Group", "ermak_pop" )."<board_title>".$pops->get("post_title"). " " .  sprintf( __("%s men", 'ermak_pop'), $pop->count) . "</board_title>".
								"</td>
							</tr>
						</table>";
						//
						$disloc_id						= SMC_Location::get_child_location_ids($id, true);
						$Ermak_Consume->do_consume( $id, self::get_type(), $disloc_id, null, $pop->type, $pop->count );
						//
					}
					//echo $html;
					break;
			}
			return array();
		}
		static function scmo_get_consume_id($consumer_type, $cont_id, $id)
		{
			switch($consumer_type)
			{
				case self::get_type():
					//echo "<p style='color:green;'>".self::get_type().", id=". $id."</p>";
					$epg		= self::get_instance($id);
					return $epg->get_consume_scheme_id();
			}
			return 0;
		}
		
		/*	
			
		*/	
		static function smco_consume_batch_filter( $meta_query, $con_id, $disloc_id, $id, $consumer_type )
		{
			global $SolingMetagameProduction;
			$loc		= SMC_Location::get_instance($con_id);			
			switch($consumer_type)
			{
				case self::get_type():
					if( !self::get_population_frontal_consume($con_id) )	break;
					$meta_query = array(
											'relation' => "AND",
											array(
													'key'		=> 'store',
													'value'		=> 1
												  )
										);
					if($SolingMetagameProduction->get_gb_value("best_before"))
					{	
						$meta_query[] 		= array(
														'key'		=> 'best_before',
														'value' 	=> 0,
														'compare'	=> '>'
													);
					}	
					if($SolingMetagameProduction->is_dislocation() && $disloc_id)
						$meta_query[]			= array( 'key'	=> 'dislocation_id', 'value'	=> $disloc_id, 'operator' => "OR");
					//echo "<p> I'm pay, I'm buy:" . Assistants::echo_me( $meta_query, true ) . "</p>";
					break;
			}
			return $meta_query;
		}
		
		//user tool page - calculator (Ermak_production/SMP_Calculator.php)
		static function smp_calc_location_needs($args, $location_id)
		{
			global $Ermak_Consume;
			$return_type			= self::get_population_frontal_consume($location_id);
			//if($return_type)		return array();//$args; // money returned to payer of goods and not to owner
			$population_groups	 	= self::get_population($location_id);
			
			$schemes				= array();
			if(count($population_groups))
				foreach($population_groups as $group)
				{
					$epg			= self::get_instance($group->type);
					$scheme_id		= $epg->get_consume_scheme_id();
					$data			= $Ermak_Consume->get_consume_data($scheme_id, $group->count);
					$dd				= array();
					foreach($data as $dat=>$val)
					{
						$d					= array();
						$d['need']			= (int)$val['need'];
						$industry			= SMP_Industry::get_instance($dat);
						$d['resorse_name'] 	= $industry->get("resourse_name");
						$d['money']			= (int)$val['money'];
						$d['evalble']		= 0;
						$d['not_evalble']	= (int)$val['need'];
						$dd[$dat]			= $d;
					}
					$dd['name']		= $epg->get_pictogramm(25) . ": <b>".$Ermak_Consume->get_consume_scheme($scheme_id)->post_title."</b> (" .  sprintf(__("%s men", "ermak_pop"), $group->count).")";
					$args[]			= $dd;
				}
			else
				null;
			
			return array_merge($args, $schemes);
			
		}
		static function smc_add_object_type($array)
		{
			$smp_industry										= array();
			$smp_industry['t']									= array('type'=>'post');
			$smp_industry['consume_scheme_id']					= array('type'=>'id', 'object'=>SMCO_CONSUME_SCHEME_TYPE);
			$smp_industry['picto_id']							= array('type'=>'media', 'download'=>true);
			$array[ERMAK_POPULATION_GROUP_TYPE]					= $smp_industry;
			
			$array[SMC_LOCATION_NAME]['population_frontal_consume']	= array('type'=>"number");
			$array[SMC_LOCATION_NAME]['population_structure']	= array(
				'type'		=> "db_row", 
				'db_name'	=> 'ermak_population', 
				"db_field"	=> array(
					"location_id"=>array(
						"type"=>"id", 
						"object"=>SMC_LOCATION_NAME
						), 
					"type"=> array(
						"type"=>"id", 
						"object" =>ERMAK_POPULATION_GROUP_TYPE
						), 
					"count"=>"number"
					)
				);
			
			
			return $array;
		}
		static function smc_add_option($options)
		{
			
			return $options;
		}
		
		static function location_display_tbl1($text, $location, $term_meta, $location_type)
		{
			$html		= "
			<div class='cell_011'>
				".
				self::get_location_population_table($location->term_id).
			"</div>";
			return $text.$html;
			
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
?>